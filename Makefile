CXX		  := g++
CXX_FLAGS := -Wall -Wextra -std=c++17 -ggdb

NVCC	:= nvcc
NVCC_FLAGS :=

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:=
EXECUTABLE	:= main


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

# gpu: clean all
# 	clear
# 	./$(BIN)/$(EXECUTABLE)

# $(BIN)/$(EXECUTABLE): $(SRC)/*.cu
# 	$(NVCC) $(NVCC_FLAGS) $(SRC)/main.cu -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*
