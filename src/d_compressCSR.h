#include <thrust/device_vector.h>
void d_compressCSR(
                   int **adj, 
                   int n, 
                   thrust::device_vector<int>& col_idxCSR, 
                   thrust::device_vector<int>& row_ptrCSR, 
                   thrust::device_vector<int>& diff_row)
{
    int k,i,j, colData = 1;
    row_ptrCSR.push_back(1);
    for(i = 1;i <= n;i++)
    {
        for(j = 1; j <= n; j++) {
            if(adj[i-1][j-1]) {
                colData++;
                col_idxCSR.push_back(j);
            }
        }

        row_ptrCSR.push_back(colData);
    }

    for(k = 0; k<row_ptrCSR.size() - 1; k++) {
        diff_row.push_back(row_ptrCSR[k+1] - row_ptrCSR[k]);
    }
}