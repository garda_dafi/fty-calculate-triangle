__global__ void calcTriangleParallel(int *col_idxCSR,
                                     int *row_ptrCSR,
                                     int row_ptrCSR_size,
                                     int *diff_rowCSR,
                                     int *count)
{
    int k = blockIdx.x, j;
    if(diff_rowCSR[k] > 0) {

        for(j = k; j < row_ptrCSR_size; j++) {

            if(col_idxCSR[row_ptrCSR[j]] > 0) {

                if(col_idxCSR[row_ptrCSR[j]] == col_idxCSR[row_ptrCSR[j]+k]) {
                    if((col_idxCSR[row_ptrCSR[j] == col_idxCSR[(row_ptrCSR[j]-1)+k]])){
                        atomicAdd(count, 1);
                    }
                }
                
            }

        }

    }
}
