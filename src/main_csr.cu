#include <cuda_runtime.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <ctime>
#include "readFileCSR.h"
#include "calcTriangleParallel.h"

using namespace std;

int main() {
    /**
     * Menyiapkan variabel untuk CSR
     */
    thrust::device_vector<int> col_idx, row_ptr, diff_row;

    /**
     * Data yang dibaca
     */
    readFileCSR("../data/test.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/moreno_zebra.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/ego-facebook.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/opsahl-powergrid.tsv", col_idx, row_ptr, diff_row);
    // readFileCSR("./data/arenas-pgp.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/reactome.tsv", col_idx, row_ptr, diff_row);

    /**
    * Menampilkan column index
    **/
    cout << "Col_idx = " << "col size " << col_idx.size() << endl;
    for(int i = 0; i<col_idx.size(); i++) {
        cout << col_idx[i] << " ";
    }
    cout << endl;

    /**
    * Menampilkan row pointer
    **/
    cout << endl << "row_ptr = " << "row size = " << row_ptr.size() << endl;
    for(int i = 0; i < row_ptr.size(); i++) {
        cout << row_ptr[i] << " ";
    }
    cout << endl;

    /**
    * Menampilkan diff row
    **/
    cout << endl << "diff_row = " << "diff size = " << diff_row.size() << endl;
    for(int i = 0; i < diff_row.size(); i++) {
        cout << diff_row[i] << " ";
    }
    cout << endl;


    /**
    * Menyiapkan data untuk kernel
    **/
    int *d_col_idx  = thrust::raw_pointer_cast(&col_idx[0]);
    int *d_row_ptr  = thrust::raw_pointer_cast(&row_ptr[0]);
    int *d_diff_row = thrust::raw_pointer_cast(&diff_row[0]);
    int *d_count = 0;
    int *h_count = new int;
    cudaMalloc((void **)&d_count, sizeof(int));
    *h_count = 0;
    cudaMemcpy(d_count, h_count, sizeof(int), cudaMemcpyHostToDevice);
    float milliseconds = 0;

    /**
     * Menghitung ukuran row_ptr dan diff_row
     */
    int row_ptr_size    = row_ptr.size();
    int diff_row_size   = diff_row.size();

    /**
     * Memulai event untuk menghitung waktu eksekusi
     */
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    /**
    * Menjalankan Kernel
    **/
    cudaEventRecord(start, 0);
    calcTriangleParallel<<<diff_row.size(),1>>>(
                                  d_col_idx,
                                  d_row_ptr,
                                  row_ptr.size(),
                                  d_diff_row,
                                  d_count);

    /**
     * Selesai menjalankan kernel, menghitung waktu ekseskusi
     * @type {String}
     */
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&milliseconds, start, stop);


    cudaMemcpy(h_count, d_count, sizeof(int), cudaMemcpyDeviceToHost);

    /**
    * Menampilkan hasil
    **/
    cout << "Total triangle = " << *h_count << endl;
    cout << "Elapsed time = " << milliseconds/(float)1000 << "s" << endl;

    free(h_count);
    cudaFree(d_count);
    return 0;
}
