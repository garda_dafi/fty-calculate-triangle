#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <time.h>
#include "adjacency_matrix.h"
#include "readFile.h"
#include "displayAdj.h"
#include "compressCSR.h"
#include "calcTriangleSerial.h"

using namespace std;

int main() {
    unsigned int i;
    // int zebraVertices = 27;
    // int opsahlPowergrid = 4941;
    // int facebookVertices = 2888;
    int testVertices = 5;
    // int reactomeVertices = 6327;
    // int arenasVertices = 10680;
    vector<int> col_idx, row_ptr;
    AdjacencyMatrix AM(testVertices);

    readFile("./data/test.tsv", AM);
    // readFile("./data/moreno_zebra.tsv", AM);
    // readFile("./data/ego-facebook.tsv", AM);
    // readFile("./data/opsahl-powergrid.tsv", AM);
    // readFile("./data/reactome.tsv", AM);
    // readFile("./data/arenas-pgp.tsv", AM);

    int **matrixPtr = AM.adjPtr();
    // displayAdj(matrixPtr, arenasVertices);

    compressCSR(matrixPtr, testVertices, col_idx, row_ptr);

    cout << "Col_idx = " << endl;
    for(i = 0; i<col_idx.size(); i++) {
        cout << col_idx[i] << " ";
    }

    cout << endl << "row_ptr = " << endl;
    for(i = 0; i<row_ptr.size(); i++) {
        cout << row_ptr[i] << " ";
    }

    cout << endl;

    const clock_t start = clock();
    int totalTriangle = calcTriangleSerial(col_idx, row_ptr);
    const clock_t end = clock() - start;

    cout << "Time elapsed = ";
    cout << float(end) / CLOCKS_PER_SEC << endl;

    cout << endl << "Total Triangle = " << totalTriangle << endl;


    return 0;
}
