using namespace std;
void readFileCSR(string fileName,
                 thrust::device_vector<int>& col_idxCSR,
                 thrust::device_vector<int>& row_ptrCSR,
                 thrust::device_vector<int>& diff_row ) {

    ifstream tsvFile;
    tsvFile.open(fileName);
    string line, vertex;
    int u = 0, uTemp = 0, v;

    if(tsvFile.is_open()) {
        cout << "File readed" << endl;
        int total = 0;
        int row_idx = 1;
        while(getline(tsvFile, line)) {
            total++;
            stringstream lineStream(line);
            int idx = 0;
            while(getline(lineStream, vertex, ' ')) {
                // cout << vertex << " ";
                stringstream vertexData(vertex);
                if(idx == 0) {
                    vertexData >> uTemp;
                    if(u != uTemp) {
                        row_ptrCSR.push_back(row_idx);
                    }
                    u = uTemp;
                } else {
                    vertexData >> v;
                    col_idxCSR.push_back(v);
                }
                idx++;
            }
            row_idx++;
        }

        for(int k = 0; k<row_ptrCSR.size() - 1; k++) {
            diff_row.push_back(row_ptrCSR[k+1] - row_ptrCSR[k]);
        }

        tsvFile.close();
    } else {
        cout << "file not readed" << endl;
    }
}
