#include <cuda_runtime.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <ctime>
#include "adjacency_matrix.h"
#include "readFile.h"
#include "displayAdj.h"
#include "d_compressCSR.h"
#include "calcTriangleParallel.h"

using namespace std;

int main() {


    // int zebraVertices = 27;
    // int opsahlPowergrid = 4941;
    // int facebookVertices = 2888;
    int testVertices = 5;
    // int arenasVertices = 10680;
    // int reactomeVertices = 6327;

    thrust::device_vector<int> col_idx, row_ptr, diff_row;
    AdjacencyMatrix AM(testVertices);

    readFile("../data/test.tsv", AM);
    // readFile("./data/moreno_zebra.tsv", AM);
    // readFile("./data/ego-facebook.tsv", AM);
    // readFile("./data/opsahl-powergrid.tsv", AM);
    // readFile("./data/arenas-pgp.tsv", AM);
    // readFile("./data/reactome.tsv", AM);

    int **matrixPtr = AM.adjPtr();
    // displayAdj(matrixPtr, arenasVertices);

    d_compressCSR(matrixPtr, testVertices, col_idx, row_ptr, diff_row);

    /**
    * Menampilkan column index
    **/
    cout << "Col_idx = " << "col size " << col_idx.size() << endl;
    for(int i = 0; i<col_idx.size(); i++) {
        cout << col_idx[i] << " ";
    }
    cout << endl;

    /**
    * Menampilkan row pointer
    **/
    cout << endl << "row_ptr = " << "row size = " << row_ptr.size() << endl;
    for(int i = 0; i < row_ptr.size(); i++) {
        cout << row_ptr[i] << " ";
    }
    cout << endl;

    /**
    * Menampilkan diff row
    **/
    cout << endl << "diff_row = " << "diff size = " << diff_row.size() << endl;
    for(int i = 0; i < diff_row.size(); i++) {
        cout << diff_row[i] << " ";
    }
    cout << endl;


    /**
    * Menyiapkan data untuk kernel
    **/
    int *d_col_idx  = thrust::raw_pointer_cast(&col_idx[0]);
    int *d_row_ptr  = thrust::raw_pointer_cast(&row_ptr[0]);
    int *d_diff_row = thrust::raw_pointer_cast(&diff_row[0]);

    int row_ptr_size    = row_ptr.size();
    int diff_row_size   = diff_row.size();
    int *d_count = 0;
    int *h_count = new int;
    cudaMalloc((void **)&d_count, sizeof(int));
    *h_count = 0;
    cudaMemcpy(d_count, h_count, sizeof(int), cudaMemcpyHostToDevice);
    float milliseconds = 0;

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    /**
    * Menjalankan Kernel
    **/

    cudaEventRecord(start, 0);
    calcTriangleParallel<<<diff_row.size(),1>>>(
                                  d_col_idx,
                                  d_row_ptr,
                                  row_ptr.size(),
                                  d_diff_row,
                                  d_count);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&milliseconds, start, stop);
    cudaMemcpy(h_count, d_count, sizeof(int), cudaMemcpyDeviceToHost);

    /**
    * Menampilkan hasil
    **/
    cout << "Total triangle = " << *h_count << endl;
    cout << "Elapsed time = " << milliseconds/(float)1000 << "s" << endl;

    free(h_count);
    free(matrixPtr);
    cudaFree(d_count);
    return 0;
}
