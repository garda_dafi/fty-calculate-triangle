void readFile(string fileName, AdjacencyMatrix Am) {
    ifstream tsvFile;
    tsvFile.open(fileName);
    string line, vertex;
    int origin, destiny;

    if(tsvFile.is_open()) {
        cout << "File readed" << endl;
        int total = 0;
        while(getline(tsvFile, line)) {
            total++;
            stringstream lineStream(line);
            int idx = 0;
            while(getline(lineStream, vertex, ' ')) {
                // cout << vertex << " ";
                stringstream vertexData(vertex);
                if(idx == 0) {
                    vertexData >> origin;
                } else {
                    vertexData >> destiny;
                }
                
                idx++;
            }
            Am.add_edge(origin, destiny);
        }
        tsvFile.close();
    } else {
        cout << "file not readed" << endl;
    }
}