int calcTriangleSerial(vector<int>& col_idxCSR, vector<int>& row_ptrCSR)
{
    unsigned int j, count = 0;
    vector<int> diffRow;
    cout << endl;
    unsigned int k = 0;

    for(k = 0; k<row_ptrCSR.size() - 1; k++) {
        diffRow.push_back(row_ptrCSR[k+1] - row_ptrCSR[k]);
    }

    // cout << "Diffrow = " << endl;

    // for(k = 0; k<diffRow.size(); k++) {
    //     cout << diffRow[k] << " ";
    // }

    for(k = 0; k < diffRow.size(); k++) {
        if(diffRow[k] > 0) {
            for(j = k; j < row_ptrCSR.size(); j++) {
                if(col_idxCSR[row_ptrCSR[j]] > 0) {
                    if(col_idxCSR[row_ptrCSR[j]] == col_idxCSR[row_ptrCSR[j]+k]) {
                        if((col_idxCSR[row_ptrCSR[j] == col_idxCSR[(row_ptrCSR[j]-1)+k]])){
                            count++;
                        }
                    }
                }
            }
        }
    }
         
    return count;
}