void compressCSR(int **adj, int n, vector<int>& col_idxCSR, vector<int>& row_ptrCSR)
{
    int i,j, colData = 1;
    row_ptrCSR.push_back(1);
    for(i = 1;i <= n;i++)
    {
        for(j = 1; j <= n; j++) {
            if(adj[i-1][j-1]) {
                colData++;
                col_idxCSR.push_back(j);
            }
        }

        row_ptrCSR.push_back(colData);
    }
}