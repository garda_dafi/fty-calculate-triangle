#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <time.h>
#include "adjacency_matrix.h"
#include "readFileCSR_c.h"
#include "displayAdj.h"
#include "compressCSR.h"
#include "calcTriangleSerial.h"

using namespace std;

int main() {
    unsigned int i;
    /**
     * Menyiapkan variabel untuk CSR
     */
    vector<int> col_idx, row_ptr, diff_row;

    /**
     * Data yang dibaca
     */
    readFileCSR("../data/test.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/moreno_zebra.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/ego-facebook.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/opsahl-powergrid.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/reactome.tsv", col_idx, row_ptr, diff_row);
    // readFile("./data/arenas-pgp.tsv", col_idx, row_ptr, diff_row);

    /**
     * Menampilkan column index
     */

     cout << "Col_idx = " << "col size " << col_idx.size() << endl;
     for(int i = 0; i<col_idx.size(); i++) {
         cout << col_idx[i] << " ";
     }
     cout << endl;

     /**
     * Menampilkan row pointer
     **/
     cout << endl << "row_ptr = " << "row size = " << row_ptr.size() << endl;
     for(int i = 0; i < row_ptr.size(); i++) {
         cout << row_ptr[i] << " ";
     }
     cout << endl;

     /**
     * Menampilkan diff row
     **/
     cout << endl << "diff_row = " << "diff size = " << diff_row.size() << endl;
     for(int i = 0; i < diff_row.size(); i++) {
         cout << diff_row[i] << " ";
     }
     cout << endl;

    const clock_t start = clock();
    int totalTriangle = calcTriangleSerial(col_idx, row_ptr);
    const clock_t end = clock() - start;

    cout << "Time elapsed = ";
    cout << float(end) / CLOCKS_PER_SEC << endl;

    cout << endl << "Total Triangle = " << totalTriangle << endl;


    return 0;
}
